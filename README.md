# NI-MVI 2023/24 SP

## [Petals to the Metal - Flower Classification on TPU](https://www.kaggle.com/competitions/tpu-getting-started)

The objective of this semestral work is to develop a machine learning model for flower classification. The focus of this task is to leverage the power of Tensor Processing Units (TPUs) for efficient and accelerated model training.

The provided training dataset consists of a large collection of images, each labeled with one out of 104 corresponding flower types. The images are diverse, containing variations in lighting, background, scale and other aspects. This variety poses a challenge for the classification task, as the model needs to distinguish between small differences in some classes while handling the complexity of others.

https://colab.research.google.com/drive/1c5572REDAXx1Fp2DhA3y2cuw8imFtx5M?authuser=0#scrollTo=2gUQbEIoUvub
